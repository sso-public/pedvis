""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""
 
from dataclasses import dataclass, field
import logging.config
from math import ceil, floor, nan
from pathlib import Path

from pubsub import pub

import NOMAD
from NOMAD.activities import LineDestination, PointDestination
from NOMAD.constants import ATTR_PED_OPT_CLASS, ATTR_PED_PED_FORCES_C_CLASS, \
    ATTR_PED_INT_C_CLASS, IN_ISOLATION_TIME_STEP, \
    IN_RANGE_STEPS_PER_IN_ISOLATION_STEP, \
    IN_COLLISION_STEPS_PER_IN_ISOLATION_STEP, \
    MAX_PED_PREF_SPEED_NM, MAX_PED_RADIUS_NM, MAX_PED_DIST_PER_STEP_NM, \
    LRCC_ITERATIVE_TYPE, LRCC_FLOODING_TYPE, FORCE_STOP_MSG_ID, \
    TIMESTEP_MSG_ID, MAX_PED_D_SHY_NM, MAX_PED_EXTENT_NM, ATTR_PED_MANAGER, \
    ACC_RED_PED
from NOMAD.general_functions import doListCeck
from NOMAD.grid_manager import GridManager
from NOMAD.local_route_choice_manager import createLocalRouteChoiceManagers, \
     CELL_SIZE
from NOMAD.output_manager import BasicFileOutputManager, \
    InMemoryOutputManager


DEF_PED_CLASS = ATTR_PED_OPT_CLASS

if NOMAD.canUseCalcPedForcesCompiledCode:
    DEF_PED_CLASS = ATTR_PED_PED_FORCES_C_CLASS
if NOMAD.canUseGetInteractionCompiledCode and DEF_PED_CLASS == ATTR_PED_OPT_CLASS:
    DEF_PED_CLASS = ATTR_PED_INT_C_CLASS

class NomadModel():
    """
    A class used to represent a NOMAD simulation model

    ...

    Attributes
    ----------
    name : str
        the name of the model
    label : str
        a formatted string of the name of the model
    walkLevels : list[WalkLevel]
        a list of all walk levels in the model 
    demandManager : DemandManager
        an instance of the the NOMAD demand manager class
    outputManager : OutputManager
        an instance of the the NOMAD output manager class
    """

    logger = logging.getLogger(__name__)

    def __init__(self, name, label, duration, walkLevels, demandManager, outputManager, parameters,
                 inIsolationTimeStep=IN_ISOLATION_TIME_STEP,
                 inRangeStepsPerInIsolationStep=IN_RANGE_STEPS_PER_IN_ISOLATION_STEP,
                 inCollisionStepsPerInIsolationStep=IN_COLLISION_STEPS_PER_IN_ISOLATION_STEP, 
                 lrcmLoadFile=None):
        '''
        Constructor
        '''
        self.name = name
        self.label = label

        self.walkLevels = self.addWalkLevels(walkLevels)
        self.demandManager = demandManager
        self.outputManager = outputManager

        self.isRunning = False
        self.forceStop = False

        # Time related things -----------------------------------------------------------------------
        if not isinstance(inRangeStepsPerInIsolationStep, int):
            raise TypeError('The inRangeStepsPerInIsolationStep must be a integer larger than 0')
        if not isinstance(inCollisionStepsPerInIsolationStep, int):
            raise TypeError('The inCollisionStepsPerInIsolationStep must be a integer larger than 0')
        self.timeInfo = TimeInfo(duration, inIsolationTimeStep, inRangeStepsPerInIsolationStep, inCollisionStepsPerInIsolationStep)

        # -------------------------------------------------------------------------------------------

        if isinstance(parameters, NomadParameters):
            self.parameters = parameters
        else:
            self.parameters = createNomadParameters(self, parameters)

        self.cropWalkableAreas(CELL_SIZE)
        self.updateLineDestinations()

        self.gridManager = GridManager(self.walkLevels, self.parameters.MAX_PED_DIST_PER_STEP, cellSize=self.parameters.GRID_CELL_SIZE,
                                       maxObstacleDist=self.parameters.GRID_MAX_OBS_DIST, maxNeighborDistance=self.parameters.GRID_MAX_NEIGHBOR_DIST)
        lrccKwargs = {}
        if lrcmLoadFile is not None:
            lrccKwargs['loadFlNm'] = lrcmLoadFile
        lrccType = LRCC_ITERATIVE_TYPE # Temp
        if lrccType == LRCC_FLOODING_TYPE:
            createLocalRouteChoiceManagers(LRCC_FLOODING_TYPE, self.walkLevels, self.outputManager, self.parameters.MAX_PED_RADIUS, **lrccKwargs)
        elif lrccType == LRCC_ITERATIVE_TYPE:
            lrccKwargs['useC'] = NOMAD.canUseConvergeCostMatrixCompiledCode
            createLocalRouteChoiceManagers(LRCC_ITERATIVE_TYPE, self.walkLevels, self.outputManager, self.parameters.MAX_PED_RADIUS, 
                                           **lrccKwargs)
        else:
            raise Exception('Unknown lrcc type "{}"'.format(lrccType))

        from NOMAD.pedestrian_manager import PedestrianManager
        self.pedestrianManager = PedestrianManager(self.timeInfo, self.parameters)

        self.demandManager.finalizeCreation(self.parameters, self.timeInfo, self.gridManager)
        for walkLevel in self.walkLevels:
            walkLevel.finalizeCreation(self.parameters, self.gridManager)

        self.logger.info('Model created with {}'.format(self.pedestrianManager))
        pub.subscribe(self.forceStopListener, FORCE_STOP_MSG_ID)

    def forceStopListener(self):
        if self.isRunning:
            self.forceStop = True

    def updateLineDestinations(self):
        for walkLevel in self.walkLevels:
            for destination in walkLevel.destinations:
                if isinstance(destination, (LineDestination, PointDestination)):
                    destination.setMaxPedDistPerStep(self.parameters.MAX_PED_DIST_PER_STEP)

    def cropWalkableAreas(self, buffer):
        for walkLevel in self.walkLevels:
            walkLevel.cropWalkableAreas(buffer)

    def addWalkLevels(self, walkLevels):
        walkLevels = doListCeck(walkLevels)
        # Check IDs
        IDs = []
        for walkLevel in walkLevels:
            if walkLevel.ID in IDs:
                raise ValueError('A walk level with ID={} already exists!'.format(walkLevel.ID))
            # Check if underlying IDs are also unique
            IDs.append(walkLevel.ID)

        return walkLevels

    def start(self):
        self.outputManager.initialize(self)
        try:            
            self.initialize()
            self.isRunning = True
            self.runSimulation()
        except Exception as err:
            self.logger.exception('Unexpected termination of the simulation loop')
            self.finish()
            raise err

    def initialize(self):
        self.timeInfo.initialize()
        self.pedestrianManager.initialize()
        self.demandManager.initialize(self.pedestrianManager)
        self.outputManager.addPeds(self.demandManager.getPedsAddedAtInit())
        for sink in self.demandManager.sinks:
                sink.empty()
        self.outputManager.addPeds(self.demandManager.getPedsAddedAtInit())
        
        self.gridManager.updateGrid([], [])
        self.outputManager.afterStarting(self)
        self.newStaticNonInteractingPeds = []
        self.newOutsideOfSimulationPeds = []
        self.newStaticPeds = []

    def finish(self):
        self.outputManager.afterFinishing(self)
        self.outputManager.finish()
        self.isRunning = False
        self.forceStop = False

    def runSimulation(self):
        while self.timeInfo.timeLeft():
            if self.timeInfo.timeInd % 100 == 0:
                self.logger.info('Time = {} s'.format(self.timeInfo.currentTime))
            self.doMajorTimeStep()
            self.timeInfo.incrementTimeInd()
            if self.forceStop:
                break

        self.finish()

    def doMajorTimeStep(self):
        pub.sendMessage(TIMESTEP_MSG_ID, timeInd=self.timeInfo.timeInd)

        for ped in self.newOutsideOfSimulationPeds:
            ped.pos = (nan, nan)
            ped.vel = (0,0)

        self.pedsRemoved = self.emptySinks()
        self.addPeds2Simulation()

        pedsLeavingTheirActivity = self.demandManager.updateEventBasedActivitiesFromSchedulers()

        pedsInteractingAgain, pedsLeavingTheirActivity = self.pedestrianManager.updatePedsPerformingActivities(pedsLeavingTheirActivity)
        self.addPedsToGrid(pedsInteractingAgain)
        
        self.pedestrianManager.calcNewPedestrianPositions(pedsLeavingTheirActivity)

        self.gridManager.updateGrid(self.pedestrianManager.movingPedestriansIterable,
                                    self.pedestrianManager.staticPedestriansIterable)
        pedsThatReachedTheirDest = self.gridManager.getPedsThatReachedDestination()

        self.newStaticNonInteractingPeds, self.newOutsideOfSimulationPeds, self.newStaticPeds = self.pedestrianManager.updatePedsReachingTheirActivities(pedsThatReachedTheirDest)

        self.removePedsFromGrid(self.newOutsideOfSimulationPeds + self.newStaticNonInteractingPeds)

        self.outputManager.afterTimeStep(self)

    def addPedsToGrid(self, pedsInteractingAgain):
        for ped in pedsInteractingAgain:
            if ped.gridCell is None:
                self.gridManager.updatePedGridCell(ped)
            else:
                self.gridManager.addPedToCell(ped)

    def emptySinks(self):
        peds2Remove = []
        for sink in self.demandManager.sinks:
            peds2Remove += sink.empty()

        self.pedestrianManager.removePedestrians(peds2Remove)

        return peds2Remove

    def addPeds2Simulation(self):
        peds2add = self.demandManager.getPeds2Add2Simulation()
        pedsAdded = self.pedestrianManager.addPedestrians(peds2add)

        for ped in pedsAdded:
            self.gridManager.updatePedGridCell(ped)            

        self.outputManager.addPeds(pedsAdded)
        return pedsAdded

    def removePedsFromGrid(self, nonInteractingPeds):
        for ped in nonInteractingPeds:
            self.gridManager.removePedFromCell(ped)

    def getModelResults(self):
        if isinstance(self.outputManager, BasicFileOutputManager):
            return self.outputManager.scenarioFilename
        elif isinstance(self.outputManager, InMemoryOutputManager):
            return self.outputManager.pedData

    @property
    def pedestrians(self):
        return self.pedestrianManager.pedestrianIterable

    @property
    def movingPedestrians(self):
        return self.pedestrianManager.movingPedestriansList

    def getExtraIterativeArgs(self):
        obstacles2reduce = []
        for walkLevel in self.walkLevels:
            for obstacle in walkLevel.obstacles:
                if obstacle.ID.startswith('table'):
                    obstacles2reduce.append(obstacle)
        
        return {'obstacles2reduce':obstacles2reduce, 'reductionSize':CELL_SIZE}

    # TEMP to support SSO output manager
    def getCurrentTime(self):
        return self.timeInfo.currentTime

    @property
    def duration(self):
        return self.timeInfo.duration

# ================================================================================================
# ================================================================================================
def createNomadParameters(nomadModel, parametersIn):
    params = DefaultNomadParameters().__dict__

    params[MAX_PED_PREF_SPEED_NM] = nomadModel.demandManager.getMaxSpeed()
    params[MAX_PED_RADIUS_NM] = nomadModel.demandManager.getMaxRadius()
    params[MAX_PED_DIST_PER_STEP_NM] = nomadModel.demandManager.getMaxSpeed() * nomadModel.timeInfo.inIsolationTimeStep
    params[MAX_PED_D_SHY_NM] = nomadModel.demandManager.getMaxDshy()
    params[MAX_PED_EXTENT_NM] = nomadModel.demandManager.getMaxPedExtent()

    for paramName, value in parametersIn.items():
        if paramName not in params:
            raise Exception('Unknown parameter "{}"!'.format(paramName))
        params[paramName] = value
        
    return NomadParameters(**params)


@dataclass(frozen=True)
class NomadParameters():
    MAX_PED_SPEED: float
    MAX_PED_PREF_SPEED: float
    MAX_PED_DIST_PER_STEP: float
    MAX_PED_RADIUS: float
    MAX_PED_D_SHY: float
    MAX_PED_EXTENT: float

    MAX_PED_ACCELERATION: float
    GRID_CELL_SIZE: float

    MAX_PED_SPEED_2: float = field(init=False)
    MAX_PED_RADIUS2: float = field(init=False)
    MAX_PED_DIAMETER: float = field(init=False)
    GRID_MAX_OBS_DIST: float = field(init=False)
    GRID_MAX_NEIGHBOR_DIST: float = field(init=False)
    MAX_PED_ISOLATION_OFFSET: int = field(init=False)
    MAX_OBS_EXTENT: float = field(init=False)
    COLLISION_INTERVAL_PED: float = field(init=False)
    COLLISION_INTERVAL_OBS: float = field(init=False)
    MAX_APPROACHING_SPEED: float = field(init=False)

    PED_MANAGER: str
    PED_CLASS: str
    ACC_CALC_FCN: str

    def __post_init__(self):
        super().__setattr__('MAX_PED_SPEED_2', self.MAX_PED_SPEED*self.MAX_PED_SPEED)
        super().__setattr__('MAX_PED_RADIUS2', self.MAX_PED_RADIUS*self.MAX_PED_RADIUS)
        super().__setattr__('MAX_PED_DIAMETER', self.MAX_PED_RADIUS+self.MAX_PED_RADIUS)
        super().__setattr__('GRID_MAX_OBS_DIST', self.MAX_PED_D_SHY + self.MAX_PED_RADIUS)
        super().__setattr__('GRID_MAX_NEIGHBOR_DIST', self.GRID_CELL_SIZE + self.MAX_PED_EXTENT)
        super().__setattr__('MAX_PED_ISOLATION_OFFSET', ceil(self.GRID_MAX_NEIGHBOR_DIST / self.GRID_CELL_SIZE))
        super().__setattr__('MAX_OBS_EXTENT', 2 * self.MAX_PED_D_SHY)
        super().__setattr__('COLLISION_INTERVAL_PED', (self.MAX_PED_EXTENT - 2 * self.MAX_PED_RADIUS) / (2 * self.MAX_PED_PREF_SPEED))
        super().__setattr__('COLLISION_INTERVAL_OBS', (self.MAX_OBS_EXTENT - self.MAX_PED_RADIUS) / self.MAX_PED_PREF_SPEED)
        super().__setattr__('MAX_APPROACHING_SPEED', 2*self.MAX_PED_PREF_SPEED)

@dataclass(frozen=True)
class DefaultNomadParameters():
    GRID_CELL_SIZE: float = 1
    MAX_PED_ACCELERATION: float = 10
    MAX_PED_SPEED: float = 3
    PED_MANAGER: str = ATTR_PED_MANAGER
    PED_CLASS: str = DEF_PED_CLASS
    ACC_CALC_FCN: str = ACC_RED_PED

# ================================================================================================
# ================================================================================================


@dataclass(frozen=True)
class TimeInfo():
    duration: float
    
    inIsolationTimeStep: float
    inRangeStepsPerInIsolationStep: int
    inCollisionStepsPerInIsolationStep: int
    
    timeInd: int = field(init=False)
    endTimeInd: int = field(init=False)
    
    currentTime:float = field(init=False)
    
    inRangeTimeStep: float = field(init=False)
    inCollisionTimeStep: float = field(init=False)
    inRangeStep: float = field(init=False)

    def __post_init__(self):
        super().__setattr__('inRangeTimeStep', self.inIsolationTimeStep / self.inRangeStepsPerInIsolationStep)
        super().__setattr__('inCollisionTimeStep', self.inIsolationTimeStep / self.inCollisionStepsPerInIsolationStep)
        super().__setattr__('inRangeStep', self.inCollisionStepsPerInIsolationStep / self.inRangeStepsPerInIsolationStep)
        super().__setattr__('endTimeInd', self.getTimeIndCeiled(self.duration))

    def initialize(self):
        super().__setattr__('timeInd', 0)
        super().__setattr__('currentTime', self.getCurrentTime())

    def timeLeft(self):
        return self.timeInd < self.endTimeInd

    def incrementTimeInd(self):
        super().__setattr__('timeInd', self.timeInd + 1)
        super().__setattr__('currentTime', self.getCurrentTime())

    def getCurrentTime(self):
        return self.getTime(self.timeInd)

    def getTimeInd(self, timeInSeconds):
        return int(floor(timeInSeconds/self.inIsolationTimeStep))

    def getNextTimeInd(self):
        return self.timeInd + 1

    def getTimeIndCeiled(self, timeInSeconds):
        return int(ceil(timeInSeconds/self.inIsolationTimeStep))

    def getTime(self, timeInd):
        return timeInd*self.inIsolationTimeStep

    def getPreviousTime(self):
        return self.getTime(self.timeInd - 1)

    def getNextTime(self):
        return self.getTime(self.timeInd + 1)
    
    @property
    def timeStep(self):
        return self.inIsolationTimeStep

# =================================================================================================
# =================================================================================================

def onlyCreate(xmlFlNm, nomadModelClass=NomadModel, pedClass=None, lrcmLoadFile=None, seed=None):
    import numpy as np
    NOMAD.NOMAD_RNG = np.random.default_rng(seed) 
    from NOMAD.xml_scenario_input import createNomadModelFromInputXml
    return createNomadModelFromInputXml(xmlFlNm, nomadModelClass, pedClass, lrcmLoadFile)

def createAndRun(xmlFlNm, nomadModelClass=NomadModel, pedClass=None, lrcmLoadFile=None, seed=None):
    nomadModel = onlyCreate(xmlFlNm, nomadModelClass, pedClass, lrcmLoadFile, seed)
    nomadModel.start()
    return nomadModel.getModelResults()

def createAndOutput(xmlFlNm, nomadModelClass=NomadModel, pedClass=None, lrcmLoadFile=None, seed=None):
    nomadModel = onlyCreate(xmlFlNm, nomadModelClass, pedClass, lrcmLoadFile, seed)
    nomadModel.outputManager.initialize(nomadModel, includePedData=False)
    nomadModel.outputManager.afterStarting(nomadModel)
    nomadModel.outputManager.finish()
    return nomadModel.getModelResults()

def createNomadModel(name, label, duration, walkLevels, demandManager, outputManager, parameters,
                 inIsolationTimeStep=IN_ISOLATION_TIME_STEP,
                 inRangeStepsPerInIsolationStep=IN_RANGE_STEPS_PER_IN_ISOLATION_STEP,
                 inCollisionStepsPerInIsolationStep=IN_COLLISION_STEPS_PER_IN_ISOLATION_STEP,
                 nomadModelClass=NomadModel, logPd=None, logName='NOMAD_model', logStreamLevel=logging.DEBUG,
                 lrcmLoadFile=None):

    createLoggers(logPd, logName, logStreamLevel)

    logger = logging.getLogger(__name__)
    try:
        nomadModel = nomadModelClass(name, label, duration, walkLevels, demandManager, outputManager, parameters,
                 inIsolationTimeStep, inRangeStepsPerInIsolationStep, inCollisionStepsPerInIsolationStep, lrcmLoadFile)
    except Exception as err:
        logger.critical(str(err.args[0]), exc_info=True)
        nomadModel = None

    return nomadModel

def createLoggers(logDir, name='NOMAD_model', streamLevel=logging.DEBUG):
    fileLoggers = [{'ID':'fileDebug', 'level':logging.DEBUG, 'file':'debug.log'},
              {'ID':'fileInfo', 'level':logging.INFO, 'file':'info.log'}]

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    if logDir is not None:
        logDir = Path(logDir)
        if not logDir.is_dir():
            logDir.mkdir()

    configDict = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'default': {
                'format': '%(asctime)s.%(msecs)03d - %(name)s - %(levelname)s - %(message)s',
                'datefmt': '%Y/%m/%d %H:%M:%S',
                'style': '%',
            },
        },
    }

    handlers = []
    handlersDict = {}

    if logDir is not None:
        for fileLogger in fileLoggers:
            logFileName = logDir.joinpath('{}_{}'.format(name, fileLogger['file']))
            handlersDict[fileLogger['ID']] = {
                'class': 'logging.handlers.RotatingFileHandler',
                'level': fileLogger['level'],
                'formatter': 'default',
                'filename': logFileName,
                'maxBytes': 1024 * 1024 * 30,
                'backupCount': 10
                }
            handlers.append(fileLogger['ID'])

    streamID = 'stream'
    handlersDict[streamID] = {
        'class': 'logging.StreamHandler',
        'level': streamLevel,
        'formatter': 'default',
        }
    handlers.append(streamID)

    configDict['handlers'] = handlersDict
    configDict['root'] = {
        'level': streamLevel,
        'handlers': handlers,
        }

    logging.config.dictConfig(configDict)
