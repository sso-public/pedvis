# PeDViS

This repository includes the PeDViS simulator package, along with the simulation experiment files presented in Atamer Balkan and Chang et al. (2023).

### 1. Static Contact Experiments

The input (`model.json`) and output (`agent_exposure.csv`) files of simulation experiments presented in **Section 3.1 Virus spread between static contacts** (Atamer Balkan and Chang et al., 2023) are provided in `pedvis/simulation_experiments/static_contacts` directory: `1-contact_intensity` folder includes the simulation experiments presented in **3.1.1 The impact of contact intensity on exposure** and visualized in Figure 2; `2-respiratory_activities` folder includes the simulation experiments presented in **3.1.2 The impact of respiratory activities on exposure** and visualized in Figure 3; and `3-interventions` folder includes the simulation experiments presented in **3.1.3. The impact of interventions on exposure** and visualized in Figure 4.

As explained in Atamer Balkan and Chang et al (2023), the experiments for static contacts does not include human movement and was conducted using only QVEmod module provided in `pedvis/corona_model` directory.

#### 1.1 Running QVEmod

To run QVEmod, follow these steps:

 1. Go to `simulation_experiments` directory and choose the experiment setting you want to simulate from the `static_contacts` folder, and copy the `model.json` file to main `pedvis` directory. To set an example, one `model.json` file already exists in `pedvis`.
 
 2. You can now run `main.py`. After some time, the `output` folder should contain the outputs of the simulator. To set an example, one set of output files already provided in the `output` folder.

More details about the input and output files are given in **3. Input and output files** section.

### 2. Case Study Experiments

The input and output files of simulation experiments presented in **Section 3.2. PeDViS application on a case study** (Atamer Balkan and Chang et al., 2023) are provided in `pedvis/simulation_experiments/case_study` directory. As explained in Atamer Balkan and Chang et al (2023), the experiments for case study include simulation of the human movement, so they were done using the complete simulator package, the details of which are given below.

#### 2.1 Running PeDViS

To run the PeDViS simulator, follow these steps:

 1. Go to `simulation_experiments` directory, choose the case study setting you want to simulate from the `case_study` folder, go to the `inputs` folder of the corresponding case study setting, copy `config.py` file to `pedvis/corona_model` directory and copy the rest of six input files to `data` folder. To set an example, one set of input files already exists in `data` folder.

 2. (Optional) Compile the NOMAD C-files to improve the performance. To do this, install `gcc`. On Windows, [winlibs](http://winlibs.com/) is recommended (make sure to select the same architecture (x86 or x86_64) as the Python you are using). On Mac or Linux, `gcc` can be installed using a package manager (i.e. [brew](https://brew.sh/) on Mac, APT on Debian/Ubuntu, etc). To compile, go to `NOMAD/C` and run:
    ```
    gcc -c -Wall -Werror -fpic calcPedForces.c
    gcc -shared -o calcPedForces.so calcPedForces.o
    gcc -c -Wall -Werror -fpic convergeCostMatrix.c
    gcc -shared -o convergeCostMatrix.so convergeCostMatrix.o
    gcc -c -Wall -Werror -fpic getInteractionData.c
    gcc -shared -o getInteractionData.so getInteractionData.o
    ```
    **NOTE on Mac, replace `-shared` by `-dynamiclib`**, and run:

    ```
    gcc -c -Wall -Werror -fpic calcPedForces.c
    gcc -dynamiclib -o calcPedForces.dylib calcPedForces.o
    gcc -c -Wall -Werror -fpic convergeCostMatrix.c
    gcc -dynamiclib -o convergeCostMatrix.dylib convergeCostMatrix.o
    gcc -c -Wall -Werror -fpic getInteractionData.c
    gcc -dynamiclib -o getInteractionData.dylib getInteractionData.o
    ```

    You should now have several *.o and *.so/*.dylib files in that folder.

 3. You can now run `simulator.py`. The first parameter should be `local`, the second parameter is the number of replications for NOMAD, the third parameter is the number of replications for QVEmod, and the fourth parameter is the seed for the random number generator. To regenerate the same case study experiments in the manuscript, the simulator should run with the following parameters:
    ```
    cd pedvis-main
    pip3 install -r requirements.txt
    python3 simulator.py local 1 1 4
    ```
 4. After some time, the data folder (`data`) should contain the outputs of the simulator. To set an example, one set of output files already exists in `data/001/001` folder.

More details about the input and output files are given in **3. Input and output files** section.

### 3. Input and output files

Input and output files for each simulation experiment in Atamer Balkan and Chang et al. (2023) are located in `pedvis/simulation_experiments` folder.

For QVEmod, the main input file is `model.json`:
- `model.json` serves as the main input of the QVEmod and also the output of the Pedestrian and Mobility model, hence serves as a bridge between the two modules. It includes the agent-based script and provides a detailed description of the simulation model. It also encompasses environmental parameters like dimensions, decay rates, and air exchange rates. The script section defines agents with attributes such as viral load, emission rates, and scripted behaviors including movements, entries, and exits. The file also contains fixture information, detailing properties like transfer efficiency and surface characteristics.

For PeDViS simulator, six different input files are required to run the simulator package:
- `scenario.json` file includes the key settings of the simulation scenario: the expected number of agent groups that visit the venue, the expected duration of their visits, the coordinates of infrastructural elements in the venue (entrance, exit, toilet, tables, etc.), and other key settings such as whether there is a mask mandate in the venue or visitors comply physical distancing rules or not.
- `model_without_agents.json` file mainly acts a template containing the key parameters related to environment, and excluding the scripts of the agents. It includes the dimensions of the space, parameters related to interaction of the virus in the environment, and the specifications of fixtures and infrastructural elements in the venue.
- `infrastructure.xml`, `simulation_scenario.xml` and `pedestrian_parameter_sets.xml` are the key inputs of NOMAD; they include the detailed coordinates of the infrastructural elements in the venue, the parameters of the simulation scenario, and the parameters for the human movement in the venue, respectively.
- `config.ini` includes the user-defined parameter files that suppress the default parameter values in the model.

In order to run a particular simulation experiment, copy the six input files in `PeDViS/simulation_experiments/` folder to `data` folder, and follow the instructions in **1. Running the simulator** section.

---
Each simulation experiment creates a set of output files within the data folder (`data`). 12 replication-specific output files are created within separate folders for each replication. For example, the outputs in `data/001/001` folder represent the results of the 1st replication for the Pedestrian and Mobility model and 1st replication for the epidemiological model QVEmod. Among the output files created, 4 replication-specific files are actively used in the analysis and visualization presented in Atamer Balkan et al. (2023):

- `agent_exposure.csv` shows the exposure rate to viral particles by each agent through aerosols and droplets (r(s)(inhalation-aerosols) and r(s)(inhalation-droplets)), and also accumulation of viral particles on agents' hands (V(s)(hand)) (Atamer Balkan et al., 2023).
- `agents.json` shows the distribution of the number of newly infected individuals in that particular replication of the simulation experiment.
- `groups.json` shows the percentage of infection risk covering the susceptible agents within the same sitting group of the infectious agent (w(event)(inside)) and the infection risk out of the group (w(event)(outside) = 1 - w(event)(inside)) (Atamer Balkan et al., 2023).

The output files that correspond to the experiment results presented in Atamer Balkan (2023) are provided in `results` directory. To investigate the results of each experiment, unzip `results/XXX-NN.zip` file. Following the similar folder hierarchy, for example, `results/DCB-01/001/001` folder includes the simulation run results of **DCB-01** experiment's 1st replication for the Pedestrian and Mobility model and 1st replication for the epidemiological model QVEmod. Recall that, for each simulation experiment, we generated 120 replications (5 for mobility behavior with the Pedestrian and Mobility model x 24 for the assignment of the infectious agent with the epidemiological model QVEmod) (Atamer Balkan et al., 2023).

The content of the other 8 output files that can be created by the simulator but not actively used in the analysis in Atamer Balkan et al. (2023) are listed below:
- `aerosol_contamination.csv`, `droplet_contamination.csv` and `surface_contamination.csv` show the contamination level of each grid cell (x,y) in aerosols, droplets and surfaces on each time step, which correspond to V_aerosols (x,y), V_droplets (x,y), and V_fomites (x,y) (Atamer Balkan et al., 2023).
- `aerosol_cumulative_exposure_differentials.csv`, `droplet_cumulative_exposure_differentials.csv` and `surface_cumulative_exposure_differentials.csv` show the non-zero contamination level of grid cells (x,y) at the end of the simulation run.
- `agent_exposure_with_positions.csv` provides the similar information with `agent_exposure.csv`, but also including the (x,y) positions of each agent.
- `trajectories.csv` file shows the location of each active agent for each time step of the Pedestrian and Mobility model, NOMAD.

### Reference article
Atamer Balkan, B., Chang, Y., Sparnaaij, M., Wouda, B., Boschma, D., Liu, Y., ... & ten Bosch, Q. (2023). The multi-dimensional challenges of controlling respiratory virus transmission in indoor spaces: Insights from the linkage of a microscopic pedestrian simulation and SARS-CoV-2 transmission model. medRxiv. doi.org/10.1101/2021.04.12.21255349. (under review in PLOS Computational Biology)
