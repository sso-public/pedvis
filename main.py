from json import loads, dumps
from corona_model.model import Model

def main(file_name):
    with open(file_name, 'r') as in_file:
        model = Model.deserialize(loads(in_file.read()))
    model.run()
    print(model.air_exposure())
    print(model.droplet_exposure())
    print(model.surface_exposure())

if __name__ == '__main__':
    main('model.json')