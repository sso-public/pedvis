import csv
import errno
import json
import logging
import random
import traceback
import math
import os
import re
import shutil
import sys
import time
import warnings

import numpy
import requests

from pathlib import Path

from NOMAD import nomad_model, canUseGetInteractionCompiledCode, canUseCalcPedForcesCompiledCode, canUseConvergeCostMatrixCompiledCode
from corona_model.config import get_config

from corona_model.model import Model
from corona_model.surfaces import Fixture, Item

import agent_exposure_converter
from constants import SERVER_URL, PRINT_RESULTS
from corona_model.writers import AerosolContaminationWriter, DropletContaminationWriter, AgentExposureWriter, \
    SurfaceContaminationWriter
from sso_output_manager import SSOOutputManager

LOCAL_GUID = 'local'

def parseParameters():
    if len(sys.argv) > 1:
        guid = sys.argv[1]

        if guid == LOCAL_GUID or re.match(r"^[0-9a-f]{8}$", guid):
            print("Using GUID: {:s}.".format(guid))

            if len(sys.argv) > 2:
                instance = sys.argv[2]

                if re.match(r"^[0-9]{1,3}$", instance):
                    instance = int(instance)

                    print("Using instance: {:d}.".format(instance))

                    if len(sys.argv) > 3:
                        replications = sys.argv[3]

                        if re.match(r"^[0-9]{1,3}$", replications):
                            replications = int(replications)

                            print("Using replications: {:d}.".format(replications))

                            if len(sys.argv) > 4:
                                if re.match(r"^-?[0-9]+$", sys.argv[4]):
                                    rnd_seed = int(sys.argv[4])
                                    print("Using random seed: {:d}".format(rnd_seed))
                                    return guid, instance, replications, rnd_seed, True
                                else:
                                    print("Provided random seed '{:s}' is not a valid format. Should be an integer. Using instance as seed: '{:d}'".format(sys.argv[4], instance))
                                    return guid, instance, replications, instance, True
                            else:
                                print("Using the instance as the random seed: {:d}".format(instance))
                                return guid, instance, replications, instance, True
                        else:
                            print("Malformed replications provided: {:s}".format(replications))

                            return guid, instance, -1, instance, False
                    else:
                        print("No replications provided.")

                        return guid, instance, -1, instance, False
                else:
                    print("Malformed instance provided: {:s}".format(instance))

                    return guid, -1, -1, -1, False
            else:
                print("No instance provided.")

                return guid, -1, -1, -1, False
        else:
            print("Malformed GUID provided: {:s}".format(guid))

            return "", -1, -1, -1, False
    else:
        print("No GUID provided.")

        return "", -1, -1, -1, False

def isLocal(guid):
    return guid == LOCAL_GUID

def getInput(guid):

    # Hack for running locally:
    if isLocal(guid):
        # If we're running the local version, we need to keep the same order of the input files as the server, as it is
        # defined in SSOS/lib/constants.php:
        inputs = [
            os.path.join("data", "scenario.json"),
            os.path.join("data", "simulation_scenario.xml"),
            os.path.join("data", "infrastructure.xml"),
            os.path.join("data", "pedestrian_parameter_sets.xml"),
            os.path.join("data", "model_without_agents.json"),
            os.path.join("data", "config.ini"),
         ]
        return inputs, True

    print("Retrieving input...")

    startTime = time.time()

    inputs = None
    success = True

    try:
        inputs = [None] * 6

        for index in range(len(inputs)):
            parameters = {
                "guid": guid,
                "input": index + 1
            }

            request = requests.get(SERVER_URL + "/php/request.php", params=parameters, timeout=10)

            request.raise_for_status()

            if "content-disposition" in request.headers:
                match = re.match(r'^attachment; filename="(.+)"$', request.headers["content-disposition"])

                if match:
                    inputs[index] = os.path.join("data", match.group(1))

                    with open(inputs[index], "wb") as file:
                        file.write(bytes(request.content))
                else:
                    raise ValueError("Malformed content disposition header.")
            else:
                raise KeyError("No content disposition header.")
    except Exception as exception:
        traceback.print_exc()

        success = False

    stopTime = time.time()

    if success:
        print("Input retrieved in {:5.3f} seconds.".format(stopTime - startTime))
    else:
        print("Input not retrieved.")

    return inputs, success

def loadScenario(inputs):
    print("Loading scenario...")

    startTime = time.time()

    scenario = None
    success = True

    try:
        with open(inputs[0], "r") as inputFile:
            scenario = json.loads(inputFile.read())
    except Exception as exception:
        traceback.print_exc()

        success = False

    stopTime = time.time()

    if success:
        print("Scenario loaded in {:5.3f} seconds.".format(stopTime - startTime))
    else:
        print("Scenario not loaded.")

    return scenario, success

def runPedestrianModel(inputs, scenario, seed_value):
    print("Running pedestrian model...")

    if canUseGetInteractionCompiledCode and canUseCalcPedForcesCompiledCode and canUseConvergeCostMatrixCompiledCode:
        print("Using compiled C classes for NOMAD")
    else:
        warnings.warn("C classes for NOMAD NOT available!! Using Python only")

    startTime = time.time()

    serializedPositions = None
    agents = None
    success = True

    try:
        model = nomad_model.onlyCreate(inputs[1], seed=seed_value)
        model.outputManager = SSOOutputManager(scenario["measures"]["maskMandate"] == "whenMoving",  "personel" in scenario and scenario["personel"]["personelInfected"])
        model.start()

        positions = model.outputManager.getPositions()

        serializedPositions = []

        for agent, jari, x, y in positions:
            serializedPositions.append({
                "Agent": agent,
                "Time": jari,
                "X": x,
                "Y": y
            })

        agents = model.outputManager.getAgents()

        if PRINT_RESULTS:
            for agent, jari, x, y in positions:
                print("agent: {:>8s}, time: {:7.1f}, x: {:5.2f}, y: {:5.2f}".format(agent, jari, x, y))

            for agent in agents.agents.values():
                print("agent: {:>8s}".format(agent.name))
                for time_step, action in agent.script.items():
                    if action.type == "enter" or action.type == "move":
                        print("  time: {:3d}, type: {:>8s}, x: {:3d}, y: {:3d}, facing: {:1s}".format(time_step, action.type, action.x, action.y, action.facing))
                    else:
                        print("  time: {:3d}, type: {:>8s}".format(time_step, action.type))

            for agent in agents.agents.values():
                print("agent: {:>8s}, group: {:>8s}".format(agent.name, agents.getGroup(agent.name)))
    except Exception as exception:
        traceback.print_exc()

        success = False

    stopTime = time.time()

    if success:
        print("Pedestrian model completed in {:5.3f} seconds.".format(stopTime - startTime))
    else:
        print("Pedestrian model did not complete.")

    return serializedPositions, agents, success

def runEpidemiologicalModel(replication, inputs, agents):
    print("Running epidemiological model...")

    startTime = time.time()

    serializedModel = None
    model = None
    success = True

    try:
        with open(inputs[4], "r") as inputFile:
            model = Model.deserialize(json.loads(inputFile.read()))

        model.agents = agents.getAgents()

        serializedModel = model.serialize()

        model.run(config_file=inputs[5])

        if PRINT_RESULTS:
            print("replication: {:d}".format(replication))

            for agent in model.agents:
                print("agent: {:>8s}, air: {:5.3f}, droplet: {:5.3f}, surface: {:5.3f}, source: {:>3s}".format(agent.name, agent.contamination_load_air, agent.contamination_load_droplet, agent.contamination_load_surface_accumulation, "yes" if agent.viral_load > 0.0 else "no"))

            for y in range(model.env.air._height):
                for x in range(model.env.air._width):
                    print("{:5.3f} ".format(model.env.air.get_aerosol(x, y)), end="")
                print()

            for y in range(model.env.air._height):
                for x in range(model.env.air._width):
                    print("{:5.3f} ".format(model.env.air.get_droplet(x, y)), end="")
                print()

            for x in range(model.env.width):
                for y in range(model.env.height):
                    for surface in model.env.surfaces[x][y]:
                        if isinstance(surface, Fixture):
                            print("fixture: {:>8s}, x: {:3d}, y: {:3d}, contamination: {:5.3f}".format(surface.name, x, y, surface.contamination_load))

            for x in range(model.env.width):
                for y in range(model.env.height):
                    for surface in model.env.surfaces[x][y]:
                        if isinstance(surface, Item):
                            print("item: {:>8s}, x: {:3d}, y: {:3d}, contamination: {:5.3f}".format(surface.name, x, y, surface.contamination_load))
    except Exception as exception:
        traceback.print_exc()

        success = False
    stopTime = time.time()

    if success:
        print("Epidemiological model completed in {:5.3f} seconds.".format(stopTime - startTime))
    else:
        print("Epidemiological model did not complete.")

    return serializedModel, success

def saveOutput(serializedPositions, serializedModel, rep, savePerRep = True):

    print("Saving output...")

    startTime = time.time()

    success = True

    if savePerRep:
        rep_dir = os.path.join("data", "{:03d}".format(rep))
    else:
        rep_dir = "data"

    Path(rep_dir).mkdir(parents=True, exist_ok=True)

    # The EPI model outputs currently in the data folder are actually for the current replication, so we move them to the output directory.
    if savePerRep:
        shutil.move(os.path.join("data", AerosolContaminationWriter.FILE_NAME), os.path.join(rep_dir, AerosolContaminationWriter.FILE_NAME))
        shutil.move(os.path.join("data", DropletContaminationWriter.FILE_NAME), os.path.join(rep_dir, DropletContaminationWriter.FILE_NAME))
        shutil.move(os.path.join("data", AgentExposureWriter.FILE_NAME), os.path.join(rep_dir, AgentExposureWriter.FILE_NAME))
        shutil.move(os.path.join("data", SurfaceContaminationWriter.FILE_NAME), os.path.join(rep_dir, SurfaceContaminationWriter.FILE_NAME))


    # Central place for all the output paths.
    outputs = [
        os.path.join(rep_dir, "trajectories.csv"),
        os.path.join(rep_dir, "model.json"),
        os.path.join(rep_dir, "agent_exposure.csv"),
        os.path.join(rep_dir, "aerosol_contamination.csv"),
        os.path.join(rep_dir, "droplet_contamination.csv"),
        os.path.join(rep_dir, "surface_contamination.csv"),
        os.path.join(rep_dir, "aerosol_cumulative_exposure_differentials.csv"),
        os.path.join(rep_dir, "droplet_cumulative_exposure_differentials.csv"),
        os.path.join(rep_dir, "surface_cumulative_exposure_differentials.csv"),
        os.path.join(rep_dir, "agents.json"),
        os.path.join(rep_dir, "groups.json")
    ]



    try:
        with open(outputs[0], "w", newline="") as file:
            writer = csv.DictWriter(file, fieldnames=["Agent", "Time", "X", "Y"])

            writer.writeheader()

            for serializedPosition in serializedPositions:
                writer.writerow(serializedPosition)

        with open(outputs[1], "w") as file:
            file.write(json.dumps(serializedModel, indent=4))
    except Exception as exception:
        traceback.print_exc()

        success = False

    stopTime = time.time()

    if success:
        print("Output saved in {:5.3f} seconds.".format(stopTime - startTime))
    else:
        print("Output not saved.")

    return outputs, success

def processOutput(outputs, agents):
    print("Processing output...")

    startTime = time.time()

    success = True

    # Define all parameters that matter for the output processing
    surfaceExposureRatio = 0.01
    dose = 1000

    airProportion = 0.1
    dropletProportion = 0.1
    surfaceProportion = 0.1

    airDose = dose / airProportion
    dropletDose = dose / dropletProportion
    surfaceDose = dose / surfaceProportion

    emissionRate = 10.0 ** 6.0

    try:
        outputDir = os.path.dirname(outputs[0])
        fileName = os.path.join(outputDir, "agent_exposure_with_positions.csv")

        # Reuse the agent exposure conversion tool.
        agent_exposure_converter.convertAgentExposures(outputs[1], outputs[2], fileName)

        with open(outputs[1], "r") as file:
            model = Model.deserialize(json.loads(file.read()))

        # Determine which agents are index patients and which agents are susceptible.
        sources = [agent.name for agent in model.agents if agent.viral_load > 0.0]
        targets = [agent.name for agent in model.agents if agent.viral_load == 0.0]

        cumulativeAirExposures = {}
        cumulativeDropletExposures = {}
        cumulativeSurfaceExposures = {}

        airExposureDifferentials = {}
        dropletExposureDifferentials = {}
        surfaceExposureDifferentials = {}

        with open(fileName, "r", newline="") as outputFile:
            reader = csv.DictReader(outputFile)

            # Note that we assume that the ticks are in chronological order.
            for row in reader:
                name = row["Agent"]
                tick = int(row["Tick"])
                x = int(row["X"])
                y = int(row["Y"])
                airExposure = float(row["Contamination Load Aerosol"])
                dropletExposure = float(row["Contamination Load Droplet"])
                cumulativeSurfaceExposure = float(row["Accumulated Contamination Load Surface"])
                surfaceExposure = float(row["Accumulated Contamination Load Surface"]) * get_config().SIMULATION_TIME_STEP * surfaceExposureRatio

                # Skip infected agents as they enter the simulation already exposed.
                if name in targets:
                    # Air exposures are not cumulative, so add the previous air exposure of this agent if necessary.
                    if name in cumulativeAirExposures:
                        cumulativeAirExposure = cumulativeAirExposures[name] + airExposure
                    else:
                        cumulativeAirExposure = airExposure

                    # Droplet exposures are not cumulative, so add the previous air exposure of this agent if necessary.
                    if name in cumulativeDropletExposures:
                        cumulativeDropletExposure = cumulativeDropletExposures[name] + dropletExposure
                    else:
                        cumulativeDropletExposure = dropletExposure

                    if name in cumulativeSurfaceExposures:
                        cumulativeSurfaceExposure = cumulativeSurfaceExposures[name] + surfaceExposure
                    else:
                        cumulativeSurfaceExposure = surfaceExposure

                    cumulativeAirExposures[name] = cumulativeAirExposure
                    cumulativeDropletExposures[name] = cumulativeDropletExposure
                    cumulativeSurfaceExposures[name] = cumulativeSurfaceExposure

                    key = (x, y)

                    # TODO negative exposure differentials appear to be possible even though they shouldn't be possible... maybe on agents with viral loads?
                    if airExposure > 0.0:
                        if key in airExposureDifferentials:
                            airExposureDifferentials[key] += airExposure
                        else:
                            airExposureDifferentials[key] = airExposure

                    if dropletExposure > 0.0:
                        if key in dropletExposureDifferentials:
                            dropletExposureDifferentials[key] += dropletExposure
                        else:
                            dropletExposureDifferentials[key] = dropletExposure

                    if surfaceExposure > 0.0:
                        if key in surfaceExposureDifferentials:
                            surfaceExposureDifferentials[key] += surfaceExposure
                        else:
                            surfaceExposureDifferentials[key] = surfaceExposure

        with open(outputs[6], "w", newline="") as airFile:
            writer = csv.DictWriter(airFile, fieldnames=["Tick", "X", "Y", "Exposure"])

            writer.writeheader()

            for (x, y), exposure in airExposureDifferentials.items():
                row = {
                    # TODO include leaked final tick so the server still works
                    "Tick": tick,
                    "X": x,
                    "Y": y,
                    "Exposure": exposure
                }

                writer.writerow(row)

        with open(outputs[7], "w", newline="") as dropletFile:
            writer = csv.DictWriter(dropletFile, fieldnames=["Tick", "X", "Y", "Exposure"])

            writer.writeheader()

            for (x, y), exposure in dropletExposureDifferentials.items():
                row = {
                    # TODO include leaked final tick so the server still works
                    "Tick": tick,
                    "X": x,
                    "Y": y,
                    "Exposure": exposure
                }

                writer.writerow(row)

        with open(outputs[8], "w", newline="") as surfaceFile:
            writer = csv.DictWriter(surfaceFile, fieldnames=["Tick", "X", "Y", "Exposure"])

            writer.writeheader()

            for (x, y), exposure in surfaceExposureDifferentials.items():
                row = {
                    # TODO include leaked final tick so the server still works
                    "Tick": tick,
                    "X": x,
                    "Y": y,
                    "Exposure": exposure
                }

                writer.writerow(row)

        if PRINT_RESULTS:
            for (x, y), exposure in airExposureDifferentials.items():
                print("x: {:3d}, y: {:3d}, cumulative air exposure differential: {:11.9f}".format(x, y, exposure))

            for (x, y), exposure in dropletExposureDifferentials.items():
                print("x: {:3d}, y: {:3d}, cumulative droplet exposure differential: {:11.9f}".format(x, y, exposure))

            for (x, y), exposure in surfaceExposureDifferentials.items():
                print("x: {:3d}, y: {:3d}, cumulative surface exposure differential: {:11.9f}".format(x, y, exposure))

            for agent, exposure in cumulativeAirExposures.items():
                print("agent: {:>8s}, cumulative air exposure: {:11.9f}".format(agent, exposure))

            for agent, exposure in cumulativeDropletExposures.items():
                print("agent: {:>8s}, cumulative droplet exposure: {:11.9f}".format(agent, exposure))

            for agent, exposure in cumulativeSurfaceExposures.items():
                print("agent: {:>8s}, cumulative surface exposure: {:11.9f}".format(agent, exposure))

        # Simulate the distribution of times that each number of infections could occur.
        rng = numpy.random.default_rng()

        risks = {name: 0 if (name not in cumulativeAirExposures) or (name not in cumulativeDropletExposures) or (name not in cumulativeSurfaceExposures) else 1.0 - math.exp(-cumulativeAirExposures[name] * emissionRate / airDose - cumulativeDropletExposures[name] * emissionRate / dropletDose - cumulativeSurfaceExposures[name] * emissionRate / surfaceDose) for name in targets}
        amounts = [sum([1 for risk in risks.values() if risk > rng.uniform()]) for _ in range(100000)]
        counts = numpy.bincount(amounts)

        # Drop down numpy's high ass resolution so that us mere mortals may serialize the counts.
        distribution = [{"index": index, "count": int(count)} for index, count in enumerate(counts)]

        # Calculate the average chance of infections occurring within a group.
        risksInGroups = {agents.getGroup(sourceName): [risk for targetName, risk in risks.items() if agents.getGroup(targetName) == agents.getGroup(sourceName)] for sourceName in sources}
        totalRisk = sum(risks.values())
        if totalRisk == 0:
            weightInsideGroup = 0
        else:
            weightInsideGroup = sum([riskInGroup for risksInGroup in risksInGroups.values() for riskInGroup in risksInGroup]) / sum(risks.values()) / len(risksInGroups)
        weightOutsideGroup = 1.0 - weightInsideGroup

        serializedAgents = {
            "sources": len(sources),
            "targets": len(targets),
            "distribution": distribution
        }

        # Use the same layout as with the agents so the server doesn't have to take special care.
        serializedGroups = {
            "sources": len(risksInGroups),
            "targets": agents.getNumberOfGroups() - len(risksInGroups),
            "distribution": [
                {
                    "index": 0,
                    "count": weightInsideGroup,
                },
                {
                    "index": 1,
                    "count": weightOutsideGroup
                }
            ]
        }

        with open(outputs[9], "w") as file:
            file.write(json.dumps(serializedAgents, indent=4))

        with open(outputs[10], "w") as file:
            file.write(json.dumps(serializedGroups, indent=4))

        for count in distribution:
            print("infections: {:3d}, count: {:6d}".format(count["index"], count["count"]))

        print("relative infections inside group:  {:11.9f}".format(weightInsideGroup))
        print("relative infections outside group: {:11.9f}".format(weightOutsideGroup))

    except Exception as exception:
        traceback.print_exc()

        success = False

    stopTime = time.time()

    if success:
        print("Output processed in {:5.3f} seconds.".format(stopTime - startTime))
    else:
        print("Output not processed.")

    return success

def postOutput(guid, instance, replication, outputs):

    if isLocal(guid):
        print("Running locally, no need to send output; skipping...")
        return True

    print("Sending output...")

    startTime = time.time()

    success = True

    try:
        for index in range(len(outputs)):
            parameters = {
                "guid": guid,
                "instance": instance,
                "replication": replication,
                "output": index + 1
            }

            headers = {
                "content-type": "application/octet-stream"
            }

            with open(outputs[index], "rb") as file:
                data = file.read()

            response = requests.post(SERVER_URL + "/php/response.php", params=parameters, headers=headers, data=data, timeout=10)

            response.raise_for_status()
    except Exception as exception:
        traceback.print_exc()

        success = False

    stopTime = time.time()

    if success:
        print("Output sent in {:5.3f} seconds.".format(stopTime - startTime))
    else:
        print("Output not sent.")

    return success

def postError(guid):
    if isLocal(guid):
        return True

    print("Sending error...")

    startTime = time.time()

    success = True

    try:
        parameters = {
            "guid": guid
        }

        response = requests.get(SERVER_URL + "/php/error.php", params=parameters, timeout=10)

        response.raise_for_status()
    except Exception as exception:
        traceback.print_exc()

        success = False

    stopTime = time.time()

    if success:
        print("Error sent in {:5.3f} seconds.".format(stopTime - startTime))
    else:
        print("Error not sent.")

    return success


print("Running simulator...")

startTime = time.time()

guid, instance, replications, rnd_seed, success = parseParameters()

random.seed(rnd_seed)
numpy.random.seed(rnd_seed)

if success:
    inputs, success = getInput(guid)

    if success:
        scenario, success = loadScenario(inputs)

        if success:
            serializedPositions, agents, success = runPedestrianModel(inputs, scenario, rnd_seed)

            if success:
                for replication in range(1, replications + 1):
                    serializedModel, success = runEpidemiologicalModel(replication, inputs, agents)

                    if success:
                        # TODO the trajectories will be the same every time
                        outputs, success = saveOutput(serializedPositions, serializedModel, replication, isLocal(guid))

                        if success:
                            success = processOutput(outputs, agents)

                            if success:
                                success = postOutput(guid, instance, replication, outputs)

                    if not success:
                        break

if not success:
   postError(guid)

stopTime = time.time()

if success:
    print("Simulator completed in {:5.3f} seconds.".format(stopTime - startTime))
else:
    print("Simulator did not complete.")
    raise RuntimeError("Simulator did not complete.")
