import os
import csv
from enum import Enum

from corona_model.config import get_config


class Writer:  # TODO: from abc and @abstractmethod

    FILE_NAME = str()

    class Field(Enum):
        pass

    @classmethod
    def fieldnames(cls):
        return [f.value for f in cls.Field]

    def __init__(self):
        self._file = open(os.path.join(get_config().OUTPUT_PATH, self.__class__.FILE_NAME), 'w', newline='')
        self._file.truncate()
        self._writer = csv.DictWriter(self._file, fieldnames=self.__class__.fieldnames())
        self._writer.writeheader()

    def close(self):
        self._file.close()
