import copy
import logging
from math import inf
import math
import random

from NOMAD.activity_scheduler import SITTING_ACTIVITY_NM, \
    WAIT_AT_BASE_AREA_ACTIVITY_NM, TOILET_ACTIVITY_NM, \
    INIT_WAIT_AT_BASE_AREA_ACTIVITY_NM
from NOMAD.constants import WALKING_ACTIVITY_NAME, QUEUEING_ACTIVITY_NAME
from NOMAD.output_manager import InMemoryOutputManager
from corona_model.actions import DoffMask, DonMask, Enter, Leave, Move
from corona_model.agent import Agent
from corona_model.config import get_config
from corona_model.environment import Environment
import numpy as np


logger = logging.getLogger(__name__)

# An output manager for the pedestrian model that directly produces agents and their scripts for the epidemiological model.
# It reuses the time step filter from the InMemoryOutputManager used by the model. All other functionality is overridden.
class SSOOutputManager(InMemoryOutputManager):
    def __init__(self, masks, staffInfected):
        super().__init__()
        # Whether to have agents wear masks while moving.
        self.masks = masks

        # Whether the source of the infection (patient zero) is part of the staff group or the other groups
        self.staffInfected = staffInfected

    def afterFinishing(self, nomadModel):
        super().afterFinishing(nomadModel)
        self.epiModelTimeStep = math.floor(3600.0 * get_config().SIMULATION_TIME_STEP)  # seconds
        print(self.epiModelTimeStep)

        self.epiModelCellSize = get_config().MOBILITY_CELL_SIZE / 100.0
        self.nomadTimeStep = nomadModel.timeInfo.timeStep
        self.endTimeTick = self.getTimeTick(nomadModel.timeInfo.duration)

        self._createPositions()
        self._createAgents()

    def finish(self):
        super().finish()
        del self.pedData

    def _createPositions(self):
        self.positions = {}
        for pedID, pedData in self.pedData.items():
            agentName = self.getAgentName(pedID)
            self.positions[pedID] = [
                (agentName,
                 float(pedData.times[ii]),
                 float(pedData.positions[ii, 0]),
                 float(pedData.positions[ii, 1])
                 )
                for ii in range(len(pedData.times))
            ]

    def _createAgents(self):
        self.agents = {}
        self.groups = {}

        for pedID, pedData in self.pedData.items():
            script = self._createAgentScript(pedData)
            # Note: the infected agent will be determined later
            self.agents[pedID] = self._createAgent(pedID, script)
            self.groups[pedID] = pedData.groupID

    def _createAgent(self, pedID, script):

        agent = Agent(name=self.getAgentName(pedID),
                      viral_load=0,
                      contamination_load_air=0,
                      contamination_load_droplet=0,
                      contamination_load_surface=0,
                      emission_rate_air=0.1947,
                      emission_rate_droplet=0.6553,
                      pick_up_air=2.34,
                      pick_up_droplet=2.34,
                      contamination_fraction=1,
                      script=script,
                      is_active=False,
                      wearing_mask=self.masks
                      )

        return agent

    def _createAgentScript(self, pedData):
        script = {}
        # Loop over the activity log
        self.previousPos = None
        self.epiTimeTicks, self.timeTick2posInd = self._getPositionIndices(pedData)

        pedIsOutsideOfSimulation = True
        pedIsSitting = False
        previousEndTimeTick = inf
        
        if pedData.endTime is not None:
            endTimeTick = self.getTimeTick(pedData.endTime)
        else:
            endTimeTick = self.getTimeTick(pedData.times[-1])
            
        for activity in pedData.activities:
            activityStartTick = self.getTimeTick(activity.startTime)
            if activity.endTime is None:
                if activityStartTick < self.epiTimeTicks[-1]:
                    activityEndTick = activityStartTick + 1
                else:
                    activityEndTick = activityStartTick
            else:
                activityEndTick = self.getTimeTick(activity.endTime)

            activityStartTickOnboundary = self.isOnBoundary(activity.startTime) 

            if activityStartTick == activityEndTick and not activityStartTickOnboundary and activityStartTick == previousEndTimeTick:
                continue

            previousEndTimeTick = activityEndTick

            if not activityStartTickOnboundary:
                activityStartTick = activityStartTick + 1

            if pedIsOutsideOfSimulation:
                if activity.activityType in (INIT_WAIT_AT_BASE_AREA_ACTIVITY_NM,):
                    continue
                self._addEnterActivity(script, activityStartTick, pedData)
                pedIsOutsideOfSimulation = False

            if pedIsSitting:
                if self.masks:
                    self._addMaskOnActivity(script, activityStartTick - 1)
                pedIsSitting = False

            if activity.activityType == WALKING_ACTIVITY_NAME:                
                for timeTick in range(activityStartTick, activityEndTick):
                    self._addMoveActivity(script, pedData, timeTick)
            elif activity.activityType in (WAIT_AT_BASE_AREA_ACTIVITY_NM, ): #TOILET_ACTIVITY_NM
                # Add leave
                self._addLeaveActivity(script, activityStartTick)
                pedIsOutsideOfSimulation = True
            elif activity.activityType == QUEUEING_ACTIVITY_NAME:
                for timeTick in range(activityStartTick, activityEndTick):
                    self._addMoveActivity(script, pedData, timeTick)
            else:
                # All static activities
                if activityStartTick not in script and not( activityStartTick in script and isinstance(script[activityStartTick], Enter)):
                    self._addMoveActivity(script, pedData, activityStartTick)  # Add a move action in case the ped was not yet moved to the activity location
                if activity.activityType == SITTING_ACTIVITY_NM:
                    if self.masks:
                        self._addMaskOffActivity(script, activityStartTick + 1)
                    pedIsSitting = True

        if len(script) == 0 or not isinstance(script[max(script.keys())], Leave):
            if pedData.endTime is not None:
                endTimeInd = self.getTimeTick(pedData.endTime)
                if pedIsSitting:
                    if self.masks:
                        self._addMaskOnActivity(script, endTimeInd - 1)
                    pedIsSitting = False
                self._addLeaveActivity(script, endTimeInd)
                pedIsOutsideOfSimulation = True

        return script

    def _addEnterActivity(self, script, timeTick, pedData):
        posInd = self.timeTick2posInd[timeTick]
        cellPos = self.getCellPos(pedData.positions[posInd])
        script[timeTick] = Enter(cellPos[0], cellPos[1],
                                facing=self.getFacingDirection(pedData.normalizedVelocities[posInd]))
        self.previousPos = cellPos

    def _addLeaveActivity(self, script, timeTick):
        script[timeTick] = Leave()

    def _addMoveActivity(self, script, pedData, timeTick):
        posInd = self.timeTick2posInd[timeTick]
        cellPos = self.getCellPos(pedData.positions[posInd])
        if self.previousPos == cellPos or timeTick in script:
            return

        script[timeTick] = Move(cellPos[0] - self.previousPos[0], cellPos[1] - self.previousPos[1],
                               facing=self.getFacingDirection(pedData.normalizedVelocities[posInd]))
        self.previousPos = cellPos

    def _addMaskOnActivity(self, script, timeTick):
        if timeTick in script:
            logger.warning(f'Cannot add MaskOn action without overwriting another action at time tick = {timeTick}')
            return
        
        script[timeTick] = DonMask()

    def _addMaskOffActivity(self, script, timeTick):
        if timeTick in script:
            logger.warning(f'Cannot add MaskOff action without overwriting another action at time tick = {timeTick}')
            return
        
        script[timeTick] = DoffMask()

    def _getPositionIndices(self, pedData):
        timeTicksVector = self.getTimeTicks(pedData.times)
        changeIndices = np.argwhere((timeTicksVector[1:] - timeTicksVector[:-1]) > 0).flatten()
        timeTicks = [int(timeTick) for timeTick in timeTicksVector]
        
        epiTimeTicks = []
        timeTick2posInd = {}

        if len(changeIndices) == 0 and pedData.endTime is None:
            if self.isOnBoundary(pedData.times[0]):
                timeTick = timeTicks[0]
                posInd = 0
            else:
                timeTick = timeTicks[0] + 1
                posInd = len(timeTicks) - 1
            return [timeTick], {timeTick: posInd}

        if self.isOnBoundary(pedData.times[0]):
            epiTimeTicks.append(timeTicks[0])
            timeTick2posInd[timeTicks[0]] = 0

        for changeInd in changeIndices:
            previousTimeInd = changeInd
            currentTimeInd = changeInd + 1
            previousTimeTick = timeTicks[previousTimeInd]
            currentTimeTick = timeTicks[currentTimeInd]
            
            epiTimeTicks.append(currentTimeTick)
            if self.isOnBoundary(pedData.times[currentTimeInd]):                    
                timeTick2posInd[currentTimeTick] = currentTimeInd
            else:
                timeTick2posInd[currentTimeTick] = previousTimeInd

            for timeTick in range(previousTimeTick+1,currentTimeTick):
                epiTimeTicks.append(timeTick)
                timeTick2posInd[timeTick] = previousTimeInd
            
        if pedData.endTime is None:
            timeTick = timeTicks[-1] + 1
            posInd = len(timeTicks) - 1
            epiTimeTicks.append(timeTick)
            timeTick2posInd[timeTick] = posInd

        return sorted(epiTimeTicks), timeTick2posInd

    def getTimeTicks(self, times):
        return np.floor(times / self.epiModelTimeStep).astype(int)
  
    def getTimeTick(self, time):
        return int(math.floor(time/self.epiModelTimeStep))
        
    def isOnBoundary(self, time):
        return self.getTimeTick(time) > self.getTimeTick(time - 0.5 * self.nomadTimeStep)

    def getFacingDirection(self, normVel):
        return Environment.get_direction(0, 0, normVel[0], normVel[1])

    def getCellPos(self, pos):
        return math.floor(pos[0] / self.epiModelCellSize), \
               math.floor(pos[1] / self.epiModelCellSize)

    def getAgentName(self, pedID):
        return "agent{:02d}".format(pedID)

    def getGroupName(self, ped):
        if ped.groupID == "staff":
            return ped.groupID
        else:
            return "group{:03d}".format(int(ped.groupID.split("_")[1]))

    def getPositions(self):
        return [position for agent in self.positions.values() for position in agent]

    def getAgents(self):

        if self.staffInfected and "staff" in self.groups.values():
            candidate_group_names = ["staff"]
        else:
            candidate_group_names = list(set(filter(lambda g: g != "staff", self.groups.values())))

        # We can leak the internal lists because the output manager will no longer be used.
        return Agents(self.agents, self.groups, candidate_group_names)

    @property
    def type(self):
        return "SSOInMemoryOutputManager"


class Agents:
    def __init__(self, agents, groups, candidate_group_names):
        self.agents = agents
        # Mapping from agent name to group ID
        self.groups = {agent.name: groups[pedID] for pedID, agent in agents.items()}
        self.groupNames = {name for name in groups.values()}
        self.candidateGroupNames = candidate_group_names

        # The IDs of the candidate agents for the next replication.
        self.candidates = {}

    def getAgents(self):
        """
        Returns a deepcopy of the agents, and makes sure that some agent is selected to be infected
        """
        if len(self.agents) > 0:
            # Make a copy of all the agents so this function can be called multiple times.
            agents = copy.deepcopy(self.agents)

            susceptible_agents = []
            for (pedID, agent) in agents.items():
                if self.getGroup(agent.name) in self.candidateGroupNames:
                    susceptible_agents.append(pedID)

            # Populate (or repopulate if there are not enough agents) the replication candidates if necessary.
            if len(self.candidates) == 0:
                self.candidates = random.sample(susceptible_agents, len(susceptible_agents))

            # Select the next replication candidate to be the source of the virus.
            candidate = self.candidates.pop()

            agents[candidate].viral_load = 1
            agents[candidate].contamination_load_surface_accumulation = 0.15

            return list(agents.values())
        else:
            return []

    def getGroup(self, agentName):
        if agentName in self.groups:
            return self.groups[agentName]
        else:
            return None

    def getGroups(self):
        return list(self.groupNames)

    def getNumberOfGroups(self):
        return len(self.groupNames)