import configparser
from dataclasses import dataclass
from functools import lru_cache
from typing import Union

_default_config: configparser.ConfigParser = configparser.ConfigParser()
_default_config['Environment'] = {
    'AirCellSize': '50',
    'MobilityCellSize': '10',
    'AgentReach': '50',
    'SimulationTimeStep': '0.00834',
    'HandwashingContaminationFraction': '0.3',
    'HandwashingEffectDuration': '0.5',
    'MaskEmissionAerosolReductionEfficiency': '0.6',
    'MaskEmissionDropletReductionEfficiency': '0.06',
    'MaskAerosolProtectionEfficiency': '0.6',
    'MaskDropletProtectionEfficiency': '0.06',
    'CleaningInterval': '8',
    'Diffusivity': '23',
    'WallAbsorbingProportion': '0.0',
    'CoughingRate': '0',
    'CoughingFactor': str(10 ** 6),
    'CoughingAerosolPercentage': '0.01',
    'CoughingDropletPercentage': '0.99',
}
_default_config['Output'] = {
    'Suppress': 'False',
    'Path': 'output',
    'AerosolContaminationWriteInterval': '1',
    'AerosolContaminationPrecision': '17',
    'DropletContaminationWriteInterval': '1',
    'DropletContaminationPrecision': '17',
    'SurfaceContaminationWriteInterval': '1',
    'SurfaceContaminationPrecision': '17',
}

_override_config_file: Union[str, None] = None


@dataclass
class Config:
    # Environment
    AIR_CELL_SIZE: int
    MOBILITY_CELL_SIZE: int
    AGENT_REACH: int
    SIMULATION_TIME_STEP: float
    HANDWASHING_CONTAMINATION_FRACTION: float
    HANDWASHING_EFFECT_DURATION: float
    MASK_EMISSION_AEROSOL_REDUCTION_EFFICIENCY: float
    MASK_EMISSION_DROPLET_REDUCTION_EFFICIENCY: float
    MASK_AEROSOL_PROTECTION_EFFICIENCY: float
    MASK_DROPLET_PROTECTION_EFFICIENCY: float
    CLEANING_INTERVAL: float
    DIFFUSIVITY: int
    WALL_ABSORBING_PROPORTION: float
    COUGHING_RATE: int
    COUGHING_FACTOR: int
    COUGHING_AEROSOL_PERCENTAGE: float
    COUGHING_DROPLET_PERCENTAGE: float
    # Output
    SUPPRESS_OUTPUT: bool
    OUTPUT_PATH: str
    AEROSOL_CONTAMINATION_WRITE_INTERVAL: int
    AEROSOL_CONTAMINATION_PRECISION: int
    DROPLET_CONTAMINATION_WRITE_INTERVAL: int
    DROPLET_CONTAMINATION_PRECISION: int
    SURFACE_CONTAMINATION_WRITE_INTERVAL: int
    SURFACE_CONTAMINATION_PRECISION: int


@lru_cache(maxsize=None)  # TODO: get_config.cache_clear() maybe on set new Config?
def get_config() -> Config:
    config = _default_config  # Start with defaults
    if _override_config_file:  # If there is an override config use it
        with open(_override_config_file) as f:
            config.read_file(f)  # Read from file overriding an options specified
    return Config(
        AIR_CELL_SIZE=config.getint("Environment", "AirCellSize"),
        MOBILITY_CELL_SIZE=config.getint("Environment", "MobilityCellSize"),
        AGENT_REACH=config.getint("Environment", "AgentReach"),
        SIMULATION_TIME_STEP=config.getfloat("Environment", "SimulationTimeStep"),
        HANDWASHING_CONTAMINATION_FRACTION=config.getfloat("Environment", "HandwashingContaminationFraction"),
        HANDWASHING_EFFECT_DURATION=config.getfloat("Environment", "HandwashingEffectDuration"),
        MASK_EMISSION_AEROSOL_REDUCTION_EFFICIENCY=config.getfloat("Environment", "MaskEmissionAerosolReductionEfficiency"),
        MASK_EMISSION_DROPLET_REDUCTION_EFFICIENCY=config.getfloat("Environment", "MaskEmissionDropletReductionEfficiency"),
        MASK_AEROSOL_PROTECTION_EFFICIENCY=config.getfloat("Environment", "MaskAerosolProtectionEfficiency"),
        MASK_DROPLET_PROTECTION_EFFICIENCY=config.getfloat("Environment", "MaskDropletProtectionEfficiency"),
        CLEANING_INTERVAL=config.getfloat("Environment", "CleaningInterval"),
        DIFFUSIVITY=config.getint("Environment", "Diffusivity"),
        WALL_ABSORBING_PROPORTION=config.getfloat("Environment", "WallAbsorbingProportion"),
        COUGHING_RATE=config.getint("Environment", "CoughingRate"),
        COUGHING_FACTOR=config.getint("Environment", "CoughingFactor"),
        COUGHING_AEROSOL_PERCENTAGE=config.getfloat("Environment", "CoughingAerosolPercentage"),
        COUGHING_DROPLET_PERCENTAGE=config.getfloat("Environment", "CoughingDropletPercentage"),
        SUPPRESS_OUTPUT=config.getboolean("Output", "Suppress"),
        OUTPUT_PATH=config.get("Output", "Path"),
        AEROSOL_CONTAMINATION_WRITE_INTERVAL=config.getint("Output", "AerosolContaminationWriteInterval"),
        AEROSOL_CONTAMINATION_PRECISION=config.getint("Output", "AerosolContaminationPrecision"),
        DROPLET_CONTAMINATION_WRITE_INTERVAL=config.getint("Output", "DropletContaminationWriteInterval"),
        DROPLET_CONTAMINATION_PRECISION=config.getint("Output", "DropletContaminationPrecision"),
        SURFACE_CONTAMINATION_WRITE_INTERVAL=config.getint("Output", "SurfaceContaminationWriteInterval"),
        SURFACE_CONTAMINATION_PRECISION=config.getint("Output", "SurfaceContaminationPrecision")
    )


def set_config(config_file):
    assert config_file
    get_config.cache_clear()  # Clear get_config cache so override will be read in
    global _override_config_file
    _override_config_file = config_file


def reset_config():
    get_config.cache_clear()
    global _override_config_file
    _override_config_file = None
